public class Dryer {
    //different characteristics for our dryers//
    private String color;
    private double price;
    private String size;
    private int minsUntilDry;
    private double heat;
    
     //prints dring process countdown//
    public void dryingProcess(int mins) {
        for(int i = mins; i <= mins && i > 0; i--) {
            System.out.println(i + " minutes until dry");
        }
        System.out.println("Done!");
    }
    
    //prints dryer finished ringtone//
    public void allDoneRingtone(){
        System.out.println("Your clothes have been succesfully dried!");
        System.out.println("Dooo-doo-dooooo  doo-doooooo-dooooo");
    }
    
    //validates heat input//
    private void validateHeat(int newHeat) {
        if(newHeat > 0.0){
            this.heat = newHeat; 
        }
    }
    //changes heat setting of you dryer//
    public void hotOrCold(int newHeat){
        validateHeat(newHeat);
    }
    
    //getter methods//
    public String getColor() {
        return this.color;
    }
    public double getPrice() {
        return this.price;
    }
    public String getSize() {
        return this.size;
    }
    public int getMinsUntilDry() {
        return this.minsUntilDry;
    }
    public double getHeat() {
        return this.heat;
    }
    
    //setter methods//
    public void setColor(String newColor) {
        this.color = newColor; 
    }
    public void setPrice(double newPrice) {
        this.price = newPrice; 
    }
    public void setSize(String newSize) {
        this.size = newSize; 
    }
    public void setMinsUntilDry(int newMinsUntilDry) {
        this.minsUntilDry = newMinsUntilDry; 
    }
    public void setHeat(double newHeat) {
        this.heat = newHeat; 
    }

    //constructor//
    public Dryer(String color, double price, String size, int minsUntilDry){
        this.color = color;
        this.price = price;
        this.size = size;
        this.minsUntilDry = minsUntilDry;
    }  
}

